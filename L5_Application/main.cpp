/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 *          FreeRTOS and stdio printf is pre-configured to use uart0_min.h before main() enters.
 *          @see L0_LowLevel/lpc_sys.h if you wish to override printf/scanf functions.
 *
 */

/*
 * Real Time Brake Assist
 * CmpE 244 Spring'15
 * Author: Naghma, Ken, Sahil, Julio
 * May 11, 2015
 */

#include <rtba.hpp>
#include "tasks.hpp"

int main()
{
    scheduler_add_task(new speed_task(PRIORITY_MEDIUM));
    scheduler_add_task(new proximity_task(PRIORITY_HIGH));
    scheduler_add_task(new calculateSBD_task(PRIORITY_MEDIUM));
    scheduler_add_task(new ui_task(PRIORITY_MEDIUM));
    scheduler_add_task(new terminalTask(PRIORITY_HIGH));

    scheduler_start();
    return -1;
}
