/*
 * 	rtba.hpp
 *
 *  Created on: May 11, 2015
 *  Author: Naghma, Ken, Sahil, Julio
 */

#ifndef RTBA_HPP_
#define RTBA_HPP_

#include "scheduler_task.hpp"
#include "shared_handles.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "can.h"
#include "uart3.hpp" // For bluetooth

class speed_task : public scheduler_task
{
    public :
        speed_task(uint8_t priority);
        bool init(void);
        bool run(void *p);

    private :
        can_t can_bus_used = can1;
        can_msg_t PID_send, OBD2_data;
        unsigned char speed_kmh = 0;
        unsigned char speed_mph = 0;
};

class proximity_task : public scheduler_task
{
    public :
        proximity_task(uint8_t priority);
        bool run(void *p);
        unsigned char cmToFt(uint32_t distanceIncm);

    private:
        uint32_t distanceINcm = 0;
        unsigned char distanceINft = 0;
};

class calculateSBD_task : public scheduler_task
{
    public :
        calculateSBD_task(uint8_t priority);
        bool run(void *p);
        unsigned char get_braking_dist(unsigned char car_speed);

    private:
        unsigned char carSpeed = 0;
        unsigned char brakingDistance = 0;
        unsigned char SBDQ_data[2] = { 0 };
};

class ui_task : public scheduler_task
{
    public :
        ui_task(uint8_t priority);
        bool init(void);
        bool run(void *p);

    private:
        unsigned char carSBD[2] = { 0 };
        unsigned char proximity = 0;
        Uart3 &u3 = Uart3::getInstance();
};

#endif /* RTBA_HPP_ */
