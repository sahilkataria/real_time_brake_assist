/*
 * rtba.cpp
 *
 *  Created on: May 11, 2015
 *      Author: Naghma, Ken, Sahil, Julio
 */

#include <rtba.hpp>
#include <stdio.h>
#include <math.h> // For round()
#include <stdlib.h>
#include "io.hpp"
#include "gpio.hpp"
#include "utilities.h"
#include "printf_lib.h"


/* This task is to send PID command to the obd2 port through a can transceiver and receive the data sent back. */
speed_task::speed_task(uint8_t priority) : scheduler_task("speedFromOBD2", 4 * 512, PRIORITY_LOW)
{
    setRunDuration(100);
}

bool speed_task::init(void)
{
	/*
	 * *** OBDII Over CAN - IMPORTANT NOTES ***
	 * 1. Cars usually have 29 bit IDs and work on 250K/500K baud
	 * 2. Broadcast ID (29 bit): 0x18DB33F1
	 * 3. Response ID (29 bit): 0x18DAF111, 0x18DAF11D
	 *
	 * 4. Hardware Pinout for SJ One Board
	 *	P2.7 = CAN2.Rx; P2.8 = CAN2.Tx
	 *	P0.0 = CAN1.Rx; P0.1 = CAN1.Tx
	 *
	 * 5. OBDII Request Format
	 * 	Bytes: 0(length of data), 1(mode), 2(PID), 3(1 or more dummy bytes)
	 *
	 * 6. OBDII Response Format
	 * 	Bytes: 0(length of data), 1(mode), 2(PID), 3(1 or more response bytes)
	 *
	 * 7. Common Modes
	 * 	Mode (hex)		Description
	 * 		01			Show current data
	 * 		02			Show freeze frame data
	 * 		03			Show stored Diagnostic Trouble Codes
	 * 		04			Clear Diagnostic Trouble Codes and stored values
	 *
	 * 8. Common PIDs
	 * 	http://en.wikipedia.org/wiki/OBD-II_PIDs
	 * 	Bytes returned are 0(A), 1(B), 2(C) and so on
	 *	PID		Bytes Returned		Description					Value (unit)
	 *	05			1				Engine coolant temperature	A-40 C
	 *	0C			2				Engine RPM					((A*256)+B)/4
	 *	0D			1				Vehicle speed				A km/h
	 *
	 */

	if(!CAN_init(can_bus_used, 500, 10, 10, NULL, NULL))
		u0_dbg_printf("speed_task: CAN Init Failed!\n");

	CAN_reset_bus(can_bus_used);
	const can_ext_id_t elist[] = { CAN_gen_eid(can_bus_used, 0x18DAF111), CAN_gen_eid(can_bus_used, 0x18DAF11D)};
	CAN_setup_filter(NULL, 0, NULL, 0, elist, 2, NULL, 0);

	PID_send.frame_fields.is_rtr = 0;
	PID_send.msg_id = 0x18DB33F1 ;
	PID_send.frame_fields.is_29bit = 1;
	PID_send.frame_fields.data_len = 4;
	PID_send.data.dwords[0] = 0x000D0102; // Least significant byte is 1st byte out

	// Set 7-segment displays on SJ One board to a known init state
	LD.setLeftDigit('h');
	LD.setRightDigit('h');

    QueueHandle_t canQ_tx = xQueueCreate(10, sizeof(char));
    addSharedObject(shared_canQ_tx, canQ_tx);

    return true;
}

bool speed_task::run(void *p)
{
	if(!CAN_tx(can_bus_used, &PID_send, 100))
		u0_dbg_printf("speed_task: CAN Tx Failed!\n");

	if(CAN_rx(can_bus_used, &OBD2_data, 100)) {
		// Example to read car's RPM
		//u0_dbg_printf("Received Message ID: %#4X\n", OBD2_data.msg_id);
		//u0_dbg_printf("PID = %#4X, RPM = %d\n", OBD2_data.data.bytes[2], ((OBD2_data.data.bytes[3] * 256)+OBD2_data.data.bytes[4])/4);

		speed_kmh = OBD2_data.data.bytes[3];
		speed_mph = round(speed_kmh * 0.621371);

		// Set 7-segment displays on SJ One board to car's speed
		LD.setNumber(speed_mph);

		// Send car's speed to the queue
		xQueueSend(getSharedObject(shared_canQ_tx), &speed_mph, portMAX_DELAY);
	}
	else {
		u0_dbg_printf("speed_task: CAN Rx Failed!\n");
		u0_dbg_printf("speed_task: TXCNT=%d  RXCNT=%d  RXDROP=%d \n", CAN_get_tx_count(can_bus_used), CAN_get_rx_count(can_bus_used),CAN_get_rx_dropped_count(can_bus_used));
	}

    return true;
}


/* This task is to read LIDAR sensor values and send those values to ProximityQueue */
proximity_task::proximity_task(uint8_t priority) : scheduler_task("distanceFromLIDAR", 4 * 512, PRIORITY_LOW)
{
    setRunDuration(100);
    QueueHandle_t proxQ_tx = xQueueCreate(10, sizeof(char));
    addSharedObject(shared_proxQ_tx, proxQ_tx);
}

bool proximity_task::run(void *p)
{
	SemaphoreHandle_t SBDSem_t = getSharedObject(shared_SBDSem_g);

	if(xSemaphoreTake(SBDSem_t, (TickType_t)0) == pdTRUE){
		uint8_t buffer[2] = { 0 };
		distanceINcm = 0;
		distanceINft = 255;

		if (I2C2::getInstance().writeReg(0xc4, 0x0, 0x4))
		{
			vTaskDelay(50);
			if(!(I2C2::getInstance().readRegisters(0xc5, 0x8f, &buffer[0], 2)))
				u0_dbg_printf("proximity_task: I2C Read Error!\n");
			else {
			    distanceINcm = (buffer[0] << 8) + buffer[1];
                distanceINft = cmToFt(distanceINcm);
			}
		}
		else
			u0_dbg_printf("proximity_task: Error Getting I2C Instance!\n");

		xQueueSend(getSharedObject(shared_proxQ_tx), &distanceINft, portMAX_DELAY);
	}
	else
		u0_dbg_printf("proximity_task: Waiting for semaphore!\n");

    return true;
}

unsigned char proximity_task::cmToFt(uint32_t distanceIncm)
{
	return round(distanceINcm * 0.0328084);
}


/* This task is to calculate the safe braking distance using data from CanBusQueue and ProximityQueue */
calculateSBD_task::calculateSBD_task(uint8_t priority) : scheduler_task("calculateSBD", 4 * 512, PRIORITY_LOW)
{
    setRunDuration(100);
    QueueHandle_t SBDQ_tx = xQueueCreate(10, sizeof(SBDQ_data));
    SemaphoreHandle_t SBDSem_g = xSemaphoreCreateBinary();
    addSharedObject(shared_SBDQ_tx, SBDQ_tx);
    addSharedObject(shared_SBDSem_g, SBDSem_g);

}

bool calculateSBD_task::run(void *p)
{
    QueueHandle_t canQ_rx;
    canQ_rx = getSharedObject(shared_canQ_tx);

    if(xQueueReceive(canQ_rx, &carSpeed, portMAX_DELAY))
    {
    	//u0_dbg_printf("SBD: Received car speed: %u\n", carSpeed);
    	if((carSpeed >= 20) && (carSpeed <= 60))
			brakingDistance = get_braking_dist(carSpeed);
    	else
    		brakingDistance = 0;

    	// Signal semaphore here to calculate proximity
		if(xSemaphoreGive(getSharedObject(shared_SBDSem_g)) != pdTRUE)
			u0_dbg_printf("calculateSBD_task: Failed to give semaphore!\n");

    	SBDQ_data[0] = carSpeed;
		SBDQ_data[1] = brakingDistance;
        xQueueSend(getSharedObject(shared_SBDQ_tx), &SBDQ_data, portMAX_DELAY);
    }
    else
    	u0_dbg_printf("calculateSBD_task: Failed to get car speed from queue!\n");

    return true;
}

/* This function returns calculated safe braking distance in unit of ft
 * param: proximity is in the unit of ft, car_speed is in the unit of mph
 */
unsigned char calculateSBD_task::get_braking_dist(unsigned char car_speed)
{
    float d;
    float u = 0.8; // friction coefficient. Nominal value on dry road is 0.8
    unsigned char g = 32; // braking deceleration 1g is 32 ft/s^2. We assume ideal case.
    float v = car_speed * 1.46667; // 1 m/h = 1.46667 f/s
    d = (v * v) / (2 * u * g);

    return round(d);
}


/* This task is merely to collect all data from different tasks and send them
* to the android app via bluetooth. The bluetooh module (RN41XV) is connected to
* SJSUone Board's Zigbee connector.
*/
ui_task::ui_task(uint8_t priority) : scheduler_task("UITask", 2 * 512, PRIORITY_LOW)
{
    setRunDuration(100);
}

bool ui_task::init(void)
{
	u3.init(115200);
	return true;
}

bool ui_task::run(void *p)
{
	QueueHandle_t SBDQ_rx, proxQ_rx;

	SBDQ_rx = getSharedObject(shared_SBDQ_tx);
	proxQ_rx = getSharedObject(shared_proxQ_tx);
    if(xQueueReceive(SBDQ_rx, &carSBD, portMAX_DELAY)){
    	if(xQueueReceive(proxQ_rx, &proximity, portMAX_DELAY)){
    		// send car speed, braking distance and proximity to android app
            u3.printf("#%u,%u,%u~", carSBD[0], carSBD[1], proximity); //speed, braking distance, proximity
    	}
    	else
    		u0_dbg_printf("ui_task: Failed to get proximity from queue!\n");
    }
    else
		u0_dbg_printf("ui_task: Failed to get SBD from queue!\n");

    return true;
}

